package dao;

import entidades.Doctor;
import entidades.Paciente;
import java.util.List;
import entidades.Persona;
import java.sql.*;
import java.util.ArrayList;
import static sql.Conexion.*;

/**
 * Gestion de personas en base de datos
 * @author Angonoa Franco
 */
public class PersonaDAO implements IPersonaDAO{
    private static final String SQL_SELECT_DOCTORES = "SELECT * FROM doctor";    
    private static final String SQL_SELECT_PACIENTES = "SELECT * FROM paciente";
    private static final String SQL_INSERT_DOCTOR = "INSERT INTO doctor(documento, nombreApellido, matricula) VALUES(?,?,?)";
    private static final String SQL_INSERT_PACIENTE = "INSERT INTO paciente(documento, nombreApellido, obraSocial) VALUES(?,?,?)";
    private static final String SQL_UPDATE_DOCTOR = "UPDATE doctor SET documento = ?, nombreApellido = ?, matricula = ? WHERE idDoctor = ?";
    private static final String SQL_UPDATE_PACIENTE = "";
    private static final String SQL_DELETE_DOCTOR = "DELETE FROM doctor WHERE idDoctor = ?";
    private static final String SQL_DELETE_PACIENTE = "";
    private static final String SQL_SEL_WHERE_DOCTOR = "";
    private static final String SQL_SEL_WHERE_PACIENTE = "";
    
    public PersonaDAO(){
        
    }

    @Override
    public int agregar(Persona per) throws SQLException{
        Connection conn = getConnection();
        PreparedStatement pstm = conn.prepareStatement(SQL_INSERT_DOCTOR);
        Doctor doc = (Doctor) per;
        pstm.setInt(1, doc.getDocumento());
        pstm.setString(2, doc.getNombreApellido());
        pstm.setInt(3, doc.getMatricula());
        int registros = pstm.executeUpdate();
        
        close(pstm);
        close(conn);
        
        return registros;
    }

    @Override
    public int modificar(Persona per) throws SQLException{
        Connection conn = getConnection();
        PreparedStatement pstm = conn.prepareStatement(SQL_UPDATE_DOCTOR);
        Doctor doc = (Doctor) per;
        pstm.setInt(1, doc.getDocumento());
        pstm.setString(2, doc.getNombreApellido());
        pstm.setInt(3, doc.getMatricula());
        pstm.setInt(4, doc.getIdDoctor());
        int registros = pstm.executeUpdate();
        
        close(pstm);
        close(conn);
        
        return registros;
    }

    @Override
    public int borrar(Persona per) throws SQLException {
        Connection conn = getConnection();
        PreparedStatement pstm = conn.prepareStatement(SQL_DELETE_DOCTOR);
        Doctor doc = (Doctor) per;
        pstm.setInt(1, doc.getIdDoctor());
        int registros = pstm.executeUpdate();
        
        close(pstm);
        close(conn);
        
        return registros;
    }

    @Override
    public Persona buscarPorDNI(Persona per) throws SQLException {
        return null;
    }

    @Override
    public List<Persona> obtenerTodasPersonas() throws SQLException{              
        Connection conn = getConnection();
        PreparedStatement pstm = conn.prepareStatement(SQL_SELECT_DOCTORES);
        ResultSet rs = pstm.executeQuery();
        List<Persona> personas = new ArrayList<>();
        
        while( rs.next() ){
            int idDoctor = rs.getInt("idDoctor");
            int documento = rs.getInt("documento");
            String nombreApe = rs.getString("nombreApellido");
            int matricula = rs.getInt("matricula");
            
            Persona persona = 
                    new Doctor(matricula, idDoctor, documento, nombreApe);
            personas.add(persona);
        }
        
        pstm = conn.prepareStatement(SQL_SELECT_PACIENTES);
        rs = pstm.executeQuery();
        
        while( rs.next() ){
            int idPaciente = rs.getInt("idPaciente");
            int documento = rs.getInt("documento");
            String nombreApe = rs.getString("nombreApellido");
            String obraSocial = rs.getString("obraSocial");
            
            Boolean obSoc = obraSocial.equalsIgnoreCase("S");
            
            Persona persona = new Paciente(obSoc, idPaciente, documento, nombreApe);
            personas.add(persona);
        }
        
        
        close(rs);
        close(pstm);
        close(conn);
        
        return personas;        
    }    
}
