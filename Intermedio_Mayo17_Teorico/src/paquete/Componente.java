package paquete;

import java.util.Objects;

public class Componente implements Comparable<Componente>{
    //ATRIBUTOS GLOBALES
    private int codigoDeBarra;
    private String marca;
    private String pais;
    private float precio;
    private static int contadorComp;

    //CONSTRUCTORES
    public Componente(int codigoDeBarra, String marca, String pais, float precio) { 
        this.codigoDeBarra = codigoDeBarra;
        this.marca = marca;
        this.pais = pais;
        this.precio = precio;
    }

    //METODOS
    public int getCodigoDeBarra() {
        return this.codigoDeBarra;
    }

    public void setCodigoDeBarra(int codigoDeBarra) {
        this.codigoDeBarra = codigoDeBarra;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "{{ Cod. de barra: " + codigoDeBarra
                + ", marca: " + marca
                + ", pais de fab.: " + pais
                + ", precio: " + precio + "$ }}";
    }

    @Override
    public int compareTo(Componente obj) {
        //return this.marca.compareTo(obj.getMarca());
        return this.codigoDeBarra - obj.getCodigoDeBarra();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.codigoDeBarra;
        hash = 59 * hash + Objects.hashCode(this.marca);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Componente other = (Componente) obj;
        if (this.codigoDeBarra != other.codigoDeBarra) {
            return false;
        }
        return Objects.equals(this.marca, other.marca);
    }

    
    

}
