package paquete;

import comparator.CodigoBarraComparator;
import comparator.MarcaComparator;
import java.util.*;

/**
 *
 * @author franc
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
                
        List<Integer> lista = new ArrayList<>();
        lista.add(1111);
        lista.add(2222);        
        
        System.out.println("Ejemplo de lista con Arraylist: " + lista);
        
        Set<Componente> hash = new HashSet<>();
        hash.add(new Componente(1, "MAR3", "ARG", 99));
        hash.add(new Componente(1, "MAR2", "ARG", 99));
        hash.add(new Componente(1, "MAR2", "ARG", 99));
        hash.add(new Componente(5, "MAR1", "ARG", 99));
        
        System.out.println("Ejemplo hash con HasSet: " + hash);
        
        Set<Componente> tree = new TreeSet<>();
        tree.add(new Componente(2, "MAR3", "ARG", 99));
        tree.add(new Componente(10, "MAR2", "ARG", 99));
        tree.add(new Componente(1, "MAR4", "ARG", 99));
        tree.add(new Componente(20, "MAR1", "ARG", 99));
        
        System.out.println("Ejemplo tree con TreeSet: " + tree);
        
        Map<String, Componente> map = new HashMap<>();
        map.put("COMP_11", new Componente(10, "MAR3", "ARG", 99));
        map.put("COMP_12", new Componente(23, "MAR4", "USA", 55));
        map.put("COMP_02", new Componente(23, "MAR4", "USA", 55));        
        map.put("COMP_02", new Componente(99, "MAR99", "USA", 55));                        
        
        Set<String> llaves = map.keySet();
        System.out.println("\nLlaves de map: " + llaves);
        
        for (String llave : llaves) {
            System.out.println("Valor de la llave " + llave + " es: " +
                    map.get(llave));
        }
        
        System.out.println("Ejemplo map con HashMap: " + map);

        List<Componente> listComp = new ArrayList<>();
        listComp.add(new Componente(10, "MAR3", "ARG", 99));
        listComp.add(new Componente(1, "MAR2", "ARG", 99));
        listComp.add(new Componente(20, "MAR5", "ARG", 99));
        listComp.add(new Componente(5, "MAR1", "ARG", 99));
        
        System.out.println("\nLISTA ANTES DEL COLECCTIONS:\n" + listComp);
        
        System.out.println("Ordenar por: ");
        System.out.println("1 - Marca");
        System.out.println("2 - Cod Barra");
        System.out.print("Ingresar: ");
        int opcion = Consola.readInt();
        
        if(opcion == 1){
            Collections.sort(listComp, new MarcaComparator());
            System.out.println("\nSE ORDENA POR MARCA");
        }else{
            Collections.sort(listComp, new CodigoBarraComparator()); 
            System.out.println("\nSE ORDENA POR CODIGO DE BARRA");
        }
        
        System.out.println("LISTA DESPUES DEL COLECCTIONS:\n" + listComp);
    }
    
}
