package comparator;

import java.util.Comparator;
import paquete.Componente;

/**
 *
 * @author franc
 */
public class MarcaComparator implements Comparator<Componente>{

    @Override
    public int compare(Componente comp1, Componente comp2) {
        return comp1.getMarca().compareTo(comp2.getMarca());
    }
    
}
