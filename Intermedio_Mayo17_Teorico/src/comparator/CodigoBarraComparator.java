
package comparator;

import java.util.Comparator;
import paquete.Componente;

/**
 *
 * @author franc
 */
public class CodigoBarraComparator implements Comparator<Componente>{

    @Override
    public int compare(Componente comp1, Componente comp2) {
        return comp1.getCodigoDeBarra() - comp2.getCodigoDeBarra();
    }
    
}
