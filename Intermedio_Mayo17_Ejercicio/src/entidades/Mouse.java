package entidades;

import constantes.EstadosComp;

public class Mouse extends Componente{
    //ATRIBUTOS GLOBALES O DE CLASE
    private boolean esInalambrico;  

    //CONSTRUCTORES    
    public Mouse(boolean esInalambrico) {
        //super();
        this.esInalambrico = esInalambrico;
    }

    public Mouse(boolean esInalambrico, String marca, String pais, 
            float precio, EstadosComp estado) {        
        super(marca, pais, precio, estado);
        this.esInalambrico = esInalambrico;
    }

    //METODOS
    public boolean getEsInalambrico() {
        return esInalambrico;
    }

    public void setEsInalambrico(boolean esInalambrico) {
        this.esInalambrico = esInalambrico;
    }
    
   
    @Override
    public String toString() {
        return super.toString() + " - Es MOUSE { Inalambrico: " + esInalambrico + "}";
    }

    @Override
    public void actualizarPrecio() {
        float impuesto = getPrecio() * 0.05f;
        setPrecio(getPrecio() + impuesto);
    }
    
}
