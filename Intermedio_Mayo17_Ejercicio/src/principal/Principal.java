package principal;

import static constantes.EstadosComp.*;
import datos.TiendaDatos;
import entidades.Teclado;
import entidades.Mouse;
import javax.swing.JOptionPane;
import negocio.ITiendaNegocio;
import negocio.TiendaNegocio;

/**
 * Clase Principal del proyecto.
 * Esta clase contiene el metodo main junto con 
 * otros metodos para la carga de objetos.
 * 
 * @author Franco
 * @version 1.1
 * @since 2023
 */
public class Principal {
    
    /**
     * Representa un objeto de la clase TiendaDatos.
     */
    private static ITiendaNegocio tien; //Solo se declara
    
    /**
     * Metodo main del proyecto.
     * Importante para ejecutar el programa.
     * 
     * @param args recibe lo ingresado desde linea de comando.
     */
    public static void main(String[] args) {        
        
        int op;
        
        //Arranca el menu de opciones
        do
        {
            System.out.println("1. Inicializar / Reiniciar componentes de mouse y teclado.");                       
            System.out.println("2. Cargar mouse y teclados.");            
            System.out.println("3. Mostrar ambos tipos de componentes");
            System.out.println("4. Actualizar precios");
            System.out.println("5. Filtrar componentes según la marca");
            System.out.println("6. Filtrar teclados según la cantidad de teclas solicitada");
            System.out.println("7. Buscar un mouse y cambiar su marca");
            System.out.println("8. Buscar un teclado y cambiar la cant. de teclas");
            System.out.println("9. Salir");
            System.out.print("\t\tIngrese: ");
            op = Consola.readInt();
            
            switch(op)
            {
                case 1:              
                        tien = new TiendaNegocio();
                        break;                      
                case 2:
                        cargarComponentes();
                        break;                        
                case 3:
                        System.out.println(tien);
                        break;               
                case 4:   
                        tien.actualizarPrecios();
                        break;                          
                case 5: String marca = JOptionPane.showInputDialog(null, "Ingrese una marca para filtrar:");
                        String filtrado = tien.filtrarPorMarca(marca);
                        System.out.println(filtrado);
                        break;                          
                case 6: String cantTeclasStr = JOptionPane.showInputDialog(null, "Ingrese la cant. teclas para filtrar:");
                        int cantTeclas = Integer.parseInt(cantTeclasStr);
                        String filtradoPorTeclas = tien.filtrarPorCantTeclas(cantTeclas);
                        System.out.println(filtradoPorTeclas);
                        break;   
                       
                case 7: String cBarraStr = JOptionPane.showInputDialog(null, "Ingrese el codigo barra:");
                        int cBarra = Integer.parseInt(cBarraStr);
                        String nvaMarca = JOptionPane.showInputDialog(null, "Ingrese la nueva marca:");                                                                        
                        String cambiarMarca = tien.cambiarMarca(cBarra, nvaMarca);
                        JOptionPane.showMessageDialog(null, cambiarMarca, "Resultado", JOptionPane.INFORMATION_MESSAGE);
                        break;   
                        
                case 8: String codBarraStr = JOptionPane.showInputDialog(null, "Ingrese el codigo barra:");
                        int codBarra = Integer.parseInt(codBarraStr);
                        String nvaCantTecStr = JOptionPane.showInputDialog(null, "Ingrese la nueva cantidad de teclas:");                                                                        
                        int nvaCantTec = Integer.parseInt(nvaCantTecStr);
                        String cambiarCantTeclas = tien.cambiarCantTeclas(codBarra, nvaCantTec);
                        JOptionPane.showMessageDialog(null, cambiarCantTeclas, "Resultado", JOptionPane.INFORMATION_MESSAGE);
                        break;   
                       
                case 9: System.out.println("GRACIAS POR UTILIZAR EL PROGRAMA");
                        break;
                default: System.out.println("Opcion incorrecta. Por favor, "
                        + "ingrese un valor entre 0 y 10.");
            }
        }
        while( op != 9 );
        
        
    } //Fin del metodo main
    
    /**
     * Carga express de componentes
     */
    public static void cargarComponentes()
    {
        tien.setComponente(new Mouse(true, "Logitech", "Argentina", 50.5f, NUEVO));
        tien.setComponente(new Mouse(false, "Razer", "EEUU", 150.5f, VENDIDO));
        tien.setComponente(new Mouse(true, "Hyperx", "Mexico", 350.5f, NUEVO));	    
        tien.setComponente(new Teclado(30, "Logitech", "Argentina", 333, DEVUELTO));
        tien.setComponente(new Teclado(35, "Corsair", "Alemania", 55.6f, REPARANDO));
        tien.setComponente(new Teclado(37, "Logitech", "Argentina", 333, NUEVO));        
    }
    
} //Fin de la clase