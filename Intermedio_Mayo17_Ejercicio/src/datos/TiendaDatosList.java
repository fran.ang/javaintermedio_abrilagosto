
package datos;

import entidades.Componente;
import entidades.Mouse;
import entidades.Teclado;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que representa a los datos de la tienda.
 * Esta clase maneja los datos a traves de una lista de Componentes.
 * 
 * @author Franco
 * @version 1.1
 * 
 */
public class TiendaDatosList implements AccesoDatosDAO{
    
    //ATRIBUTOS GLOBALES
    
    private String nombre;
    private List<Componente> componentes;
    private static TiendaDatosList instancia;
    
    //CONSTRUCTORES
    /**
     * Consctructor por defecto.
     * Este constructor se carga un objeto por defecto
     * con un nombre, y con dos teclados y mouse.
     */
    public TiendaDatosList(){
        nombre = "Informatica Cordoba";
        componentes = new ArrayList<>();      
    }

    /**
     * Metodo para el patron Singleton
     */
    public static TiendaDatosList getInstancia(){
        if(instancia == null){
            instancia = new TiendaDatosList();
        }
        return instancia;
    }

    @Override
    public int getCantidadComponentes() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void setComponente(Componente comp) {
        componentes.add(comp);
    }

    @Override
    public void actualizarPrecios() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String filtrarPorMarca(String marca) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String filtrarPorCantTeclas(int cantTeclas) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String cambiarMarca(int codBarra, String nuevaMarca) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String cambiarCantTeclas(int codBarra, int nvaCantTeclas) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String consultarComponentes() {
        String cadena = "Tienda: " + nombre + "\nMouse disponibles:\n\t";

        //Recorrer arreglo mouses
        for (Componente comp : componentes) {
            if(comp instanceof Mouse){
                cadena += comp + "\n\t";
            }
        }
        
        cadena += "\nTeclados disponibles:\n\t";
        
        //Recorrer arreglo teclados
        for (Componente comp : componentes) {
            if (comp instanceof Teclado) {
                cadena += comp + "\n\t";
            }
        }
        
        return cadena;
    }
    
}
