 
package dao;

import entidades.Persona;
import excepciones.PersonaCheckException;
import excepciones.PersonaUncheckException;
import java.util.List;

/**
 * Interfaz que respeta el patron DAO.
 * Donde todas aquellas clases que lo implanten deberan cumplir
 * con los metodos y sus respectivas firmas.
 * @author fnang
 */
public interface IPersonaDAO {
    
    public void agregar(Persona per) throws PersonaCheckException;
    
    public void modificar(Persona per, String nvoNombre) throws PersonaCheckException;
    
    public void borrar(int documento) throws PersonaCheckException;
    
    public Persona buscarPorDNI(int documento) throws PersonaCheckException;
    
    public List<Persona> obtenerTodasPersonas() throws PersonaCheckException;
}
