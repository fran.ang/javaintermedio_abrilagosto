package negocio;

import dao.IPersonaDAO;
import dao.PersonaList;
import entidades.Persona;
import excepciones.PersonaCheckException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author franc
 */
public class HospitalControlador implements IHospitalControlador{
    
    private IPersonaDAO personaDao;

    public HospitalControlador() {
        personaDao = new PersonaList();
    }        

    @Override
    public String agregarPersona(Persona persona) {        
        try {
            //Llama al DAO
            personaDao.agregar(persona);
            return "Pesona cargada con exito.";
        } catch (PersonaCheckException ex) {
            return "OCURRIO UN ERROR AL QUERER CARGAR UNA PERSONA";
        } catch (Exception ex) {
            return "OCURRIO UN ERROR: " + ex.getMessage();
        }        
    }

    @Override
    public String modificarPersona(int documento, String nuevoNombre) {        
        try {
            Persona perBusc = personaDao.buscarPorDNI(documento);
            if (perBusc != null) {
                personaDao.modificar(perBusc, nuevoNombre);
                return "Se modifico con éxito";
            }
        } catch (PersonaCheckException ex) {
            return "ERROR AL MODIFICAR EL NOMBRE Y APELLIDO";
        }
        return "No se ha podido modificar";
    }

    @Override
    public String borrarPersona(int documento) {
        try {
            personaDao.borrar(documento);
            return "Se borro la persona con exito.";
        } catch (PersonaCheckException ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String buscarPorDNI(int documento) {
        try {
            Persona perBusc = personaDao.buscarPorDNI(documento);
            return "El nombre de la persona es: " + perBusc.getNombreApellido();
        } catch (PersonaCheckException ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String obtenerTodasPersonas() {
        List<Persona> personas;
        try {
            personas = personaDao.obtenerTodasPersonas();
            if (!personas.isEmpty()) {
                return "LISTADO DE PERSONAS REGISTRADAS: \n" + personas;
            }
        } catch (PersonaCheckException ex) {
            return ex.getMessage();
        }

        return "AÚN NO HAY PERSONAS REGISTRADAS.\n";
    }
    
}
