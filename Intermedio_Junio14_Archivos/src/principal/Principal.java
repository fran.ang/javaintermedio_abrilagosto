package principal;

import entidades.Doctor;
import entidades.Paciente;
import entidades.Persona;
import dao.*;
import javax.swing.JOptionPane;
import negocio.HospitalControlador;
import negocio.IHospitalControlador;

/**
 * Clase Principal. Encontraremos todo lo
 * que el usuario verá por pantalla.
 *
 * @author fnang
 */
public class Principal {
    
    private static IHospitalControlador hospital;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        hospital = new HospitalControlador();
        menuDeOpciones();
    }

    public static void menuDeOpciones() {
        int opciones;

        do {
            System.out.println("================================");
            System.out.println("1 - Cargar personas");
            System.out.println("2 - Modificar persona");
            System.out.println("3 - Dar de baja a persona");
            System.out.println("4 - Buscar persona por documento");
            System.out.println("5 - Listar todas las personas");
            System.out.println("6 - Listar todos los doctores ordenados");
            System.out.println("7 - Listar todos los pacientes ordenados");
            System.out.println("8 - Salir");
            System.out.println("================================");
            System.out.println("Ingresar opcion: ");
            opciones = Consola.readInt();

            switch (opciones) {
                case 1: 
                    Persona nuevaPersona = cargarPersona();
                    String msjeAgregar = hospital.agregarPersona(nuevaPersona);
                    System.out.println(msjeAgregar);
                    //System.out.println(hospital.agregarPersona(cargarPersona()));
                    break;
                case 2:                    
                    int doc = solicitarDocumento();
                    System.out.println("Ingresar el segundo nombre de la persona");
                    String segNom = Consola.readLine();
                    String mensaje = hospital.modificarPersona(doc, segNom);
                    System.out.println(mensaje);
                    break;
                case 3:                    
                    int docBorrar = solicitarDocumento();
                    System.out.println(hospital.borrarPersona(docBorrar));
                    break;
                case 4:
                    int docBuscar = solicitarDocumento();
                    System.out.println(hospital.buscarPorDNI(docBuscar));
                    break;
                case 5:
                    //System.out.println(hospital.obtenerTodasPersonas());
                    JOptionPane.showMessageDialog(null, hospital.obtenerTodasPersonas(), "Listado de personas", JOptionPane.INFORMATION_MESSAGE);
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    JOptionPane.showMessageDialog(null, "Muchas gracias por utilizar el programa.", "Adios!!", JOptionPane.INFORMATION_MESSAGE);
                    break;    
            }

        } while (opciones != 8);
    } //Fin del metodo menuDeOpciones()

    public static Persona cargarPersona() {

        System.out.println("Ingrese el documento: ");
        int documento = Consola.readInt();

        System.out.println("Ingrese nombre y apellido: ");
        String nomApe = cargarNombre();

        System.out.println("¿Es un (P)aciente o un (D)octor?");
        String letra = Consola.readLine();
        switch (letra.toUpperCase()) {
            case "P":
                System.out.println("Ingrese el nro. de obra social: ");
                int obraSocial = Consola.readInt();
                return new Paciente(obraSocial, documento, nomApe);
            case "D":
                System.out.println("Ingrese la matricula: ");
                int mat = Consola.readInt();
                return new Doctor(mat, documento, nomApe);
            default:                
                return null;
        }
    } //Fin de cargarPersona()
    
    public static String cargarNombre(){
        String nomApe = Consola.readLine();
        boolean nomVal = validarNombreApellido(nomApe);
        while(!nomVal){
            System.out.println("El nombre y apellido NO ES VÁLIDO. Por favor, ingrese un nombre y apellido valido: ");
            nomApe = Consola.readLine();
            nomVal = validarNombreApellido(nomApe);
        }
        return nomApe;
    }
    
    public static boolean validarNombreApellido(String nombre){
        return nombre.matches("^([A-Z]{1}[a-z]+[ ]?){1,2}$");
    }
    
    public static int solicitarDocumento(){
        System.out.println("Ingrese el documento de la persona: ");
        return Consola.readInt();
    }

} //Fin de la clase
