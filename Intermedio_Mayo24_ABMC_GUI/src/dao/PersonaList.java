package dao;

import java.util.ArrayList;
import java.util.List;
import entidades.Persona;
import excepciones.PersonaCheckException;
import excepciones.PersonaUncheckException;
import java.util.StringTokenizer;

/**
 * Carga en memoria por ArrayList que implanta un DAO.
 * @author Angonoa Franco
 */
public class PersonaList implements IPersonaDAO{
    private List<Persona> arregloPersona;
    
    public PersonaList(){
        arregloPersona = new ArrayList<>();
    }

    @Override
    public void agregar(Persona per) throws PersonaCheckException{
        if (per != null) {
            arregloPersona.add(per);
        }
    }

    @Override
    public void modificar(Persona per, String nvoNombre) throws PersonaCheckException {
        String nombreCompleto = per.getNombreApellido();

        StringBuilder builder = new StringBuilder(nombreCompleto);
        builder.insert(nombreCompleto.indexOf(' ') + 1, nvoNombre + " ");

        String nuevoNomApe = builder.toString();

        per.setNombreApellido(nuevoNomApe);
    }

    @Override
    public void borrar(int documento) throws PersonaCheckException{
        Persona perBorrar = buscarPorDNI(documento);
        if(perBorrar != null){
            arregloPersona.remove(perBorrar);
        }
    }

    @Override
    public Persona buscarPorDNI(int documento) throws PersonaCheckException{
        for (Persona persona : arregloPersona) {
            if(persona.getDocumento() == documento){
                return persona;
            }
        }
        throw new PersonaCheckException("NO SE HA ENCONTRADO LA PERSONA.");
    }

    @Override
    public List<Persona> obtenerTodasPersonas() throws PersonaCheckException{              
        return arregloPersona;
    }    
}
