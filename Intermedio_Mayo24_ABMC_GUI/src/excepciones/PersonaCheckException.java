
package excepciones;

/**
 * EXCEPCION COMPROBADA PARA PERSONA.
 * @author franc
 */
public class PersonaCheckException extends Exception{

    public PersonaCheckException() {
    }

    public PersonaCheckException(String string) {
        super(string);
    }
        
}
