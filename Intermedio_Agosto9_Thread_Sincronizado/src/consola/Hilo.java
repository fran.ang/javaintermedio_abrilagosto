package consola;

/**
 *
 * @author Franco
 */
public class Hilo implements Runnable{
        
    private Thread hilo;
    private Integer[] numeros;
    private int sumaTotal;
    private static SumarArreglos sumaArreglos;

    //Constructor
    public Hilo(String nombreHilo, Integer[] numeros) {     
        this.sumaArreglos = new SumarArreglos();
        hilo = new Thread(this, nombreHilo);        
        this.numeros = numeros;        
    }
    
    public int getSuma(){
        return sumaTotal;
    }
    
    //Metodo de la clase para declarar e instanciar nuevos hilos
    public static Hilo crearEInstanciar(String nombre, Integer numeros[]){
        Hilo miHilo = new Hilo(nombre, numeros);
        miHilo.hilo.start(); //Inicia el hilo
        return miHilo;
    }

    @Override
    public void run() {
        System.out.println(hilo.getName() +  " iniciado");
        
//        synchronized(sumaArreglos){
            sumaTotal = sumaArreglos.sumarArreglos(numeros);
//        }        
        
        System.out.println("Suma para " + hilo.getName() + " es " + sumaTotal);
        System.out.println(hilo.getName() + " terminado");
    }
    
    
    
}
