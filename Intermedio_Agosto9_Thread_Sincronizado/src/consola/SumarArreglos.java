package consola;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Franco
 */
public class SumarArreglos {
    
    private int sumaTotal;
    
    public synchronized int sumarArreglos(Integer numeros[]){
        sumaTotal = 0;
        
        for (int cadaNumero : numeros) {
            sumaTotal += cadaNumero; //Acumulamos cada numero del arreglo
            
            System.out.printf("El total acumulado de #%s es: %d%n", 
                    Thread.currentThread().getName(), sumaTotal);
            
            try {
                Thread.sleep(1000); 
                //Detiene al hilo que es este ejecutando por un segundo
            } catch (InterruptedException ex) {
                ex.printStackTrace(System.out);
            }            
        }        
        return sumaTotal;   
    }
    
}
