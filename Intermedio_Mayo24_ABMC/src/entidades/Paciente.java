package entidades;

/**
 * Clase Paciente del modelo de negocio Representa a todos los pacientes del
 * sistema. Implementa un compareTo() que ordena por nroObraSocial.
 *
 * @author fnang
 */
public class Paciente extends Persona implements Comparable<Paciente> {

    private int nroObraSocial;

    public Paciente(int nroObraSocial, int documento, String nombreApellido) {
        super(documento, nombreApellido);
        this.nroObraSocial = nroObraSocial;
    }

    public int getNroObraSocial() {
        return nroObraSocial;
    }

    public void setNroObraSocial(int nroObraSocial) {
        this.nroObraSocial = nroObraSocial;
    }

    @Override
    public String toString() {
        return "\nPaciente: Nro. ObraSocial: " + nroObraSocial 
                + "\t " + super.toString();
    }

    @Override
    public int compareTo(Paciente pac) {
        return this.nroObraSocial - pac.getNroObraSocial();
    }

}
