
package excepciones;

/**
 * EXCEPCION NO COMPROBADAS PARA PERSONA
 * @author franc
 */
public class PersonaUncheckException extends RuntimeException{

    public PersonaUncheckException() {
    }

    public PersonaUncheckException(String string) {
        super(string);
    }
    
}
