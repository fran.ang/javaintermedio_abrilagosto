/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testthread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Franco
 */
public class ThreadInterface implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {            
            try {
                Thread.sleep(100);
                System.out.println("Hilo " + Thread.currentThread().getName() + " #" + i);

            } catch (InterruptedException ex) {
                ex.printStackTrace(System.out);
                System.out.println("HILO INTERRUMPIDO: " + Thread.currentThread().toString());
                break;
            }
        }
    }

}
