/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testthread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Franco
 */
public class ThreadHerencia extends Thread {

    public ThreadHerencia(String nombre) {
        super(nombre);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
                System.out.println("Hilo " + Thread.currentThread().getName() + " #" + i);
            } catch (InterruptedException ex) {
                ex.printStackTrace(System.out);
                System.out.println("HILO INTERRUMPIDO: " + Thread.currentThread().toString());
                break;
            }
        }
    }

}
