package negocio;

import dao.IPersonaDAO;
import dao.PersonaDAO;
import entidades.Persona;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author franc
 */
public class HospitalControlador implements IHospitalControlador{
    
    private IPersonaDAO personaDao;

    public HospitalControlador() {
        personaDao = new PersonaDAO();
    }        

    @Override
    public String agregarPersona(Persona persona) {        
        try {
            int result = personaDao.agregar(persona);
            return result > 0 ? "SE AGREGÓ UNA PERSONA CON ÉXITO" : "NO SE AGREGO LA PERSONA";
        } catch (SQLException ex) {
            return "OCURRIO UN PROBLEMA AL AGREGAR UNA PERSONA: "+ ex.getMessage();
        }
    }

    @Override
    public String modificarPersona(Persona per) {        
        try {
            int result = personaDao.modificar(per);
            return result > 0 ? "SE MODIFICÓ UNA PERSONA CON ÉXITO" : "NO SE MODIFICÓ LA PERSONA";
        } catch (SQLException ex) {
            return "OCURRIO UN PROBLEMA AL MODIFICAR UNA PERSONA: "+ ex.getMessage();
        }
    }

    @Override
    public String borrarPersona(Persona per) {
        try {
            int result = personaDao.borrar(per);
            return result > 0 ? "SE BORRÓ UNA PERSONA CON ÉXITO" : "NO SE BORRÓ LA PERSONA";
        } catch (SQLException ex) {
            return "OCURRIO UN PROBLEMA AL BORRAR UNA PERSONA: "+ ex.getMessage();
        }
    }

    @Override
    public String buscarPorDNI(int documento) {
        return null;
    }

    @Override
    public String obtenerTodasPersonas() {
        List<Persona> personas;
        
        try {
            personas = personaDao.obtenerTodasPersonas();
            if (!personas.isEmpty()) {
                return "LISTADO DE PERSONAS REGISTRADAS: \n" + personas;
            }
        } catch (SQLException ex) {
            return "OCURRIO UN ERROR: " + ex.getMessage();
        }

        return "AÚN NO HAY PERSONAS REGISTRADAS.\n";
    }
    
}
