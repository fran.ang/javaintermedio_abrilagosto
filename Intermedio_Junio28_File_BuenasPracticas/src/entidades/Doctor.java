package entidades;

import java.io.Serializable;

/**
 * Clase Doctor del modelo de negocio. Representa todo objeto que sea un doctor.
 * Implementa un compareTo() donde ordena por nro. de matricula.
 *
 * @author fnang
 */
public class Doctor extends Persona implements Comparable<Doctor>, Serializable {

    private int matricula;

    public Doctor(int matricula, int documento, String nombreApellido) {
        super(documento, nombreApellido);
        this.matricula = matricula;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    @Override
    public String toString() {
        return "\nDoctor: Matricula: " + matricula 
                + "\t " + super.toString();
    }

    @Override
    public int compareTo(Doctor doc) {
        return this.matricula - doc.getMatricula();
    }

}
