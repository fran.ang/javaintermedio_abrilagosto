package negocio;

import dao.IPersonaDAO;
import dao.PersonaList;
import entidades.Doctor;
import entidades.Paciente;
import java.io.IOException;
import java.util.List;
import utils.MensajeUtil;

/**
 *
 * @author franc
 */
public class HospitalControlador implements IHospitalControlador{
    
    private final IPersonaDAO personaDao;

    public HospitalControlador() {
        personaDao = new PersonaList();
    }        

    @Override
    public String crearArchivo(String titulo, String contenido) {        
        try {
            personaDao.crearArchivo(titulo, contenido);
            return MensajeUtil.MENSAJE_OK;
        } catch (IOException ex) {
            return MensajeUtil.MENSAJE_ERROR + ex.getMessage();
        }        
    }

    @Override
    public String leerArchivo(String titulo) {        
        try {
            return personaDao.leerArchivo(titulo);
        } catch (IOException ex) {
            return MensajeUtil.MENSAJE_ERROR + ex.getMessage();
        }
    }

    @Override
    public String serializarPaciente(String titulo) {
        try {
            personaDao.serializarPaciente(titulo);
            return MensajeUtil.MENSAJE_OK;
        } catch (IOException ex) {
            return MensajeUtil.MENSAJE_ERROR + ex.getLocalizedMessage();
        }
    }

    @Override
    public String deserializarPaciente(String titulo) {        
        try {
            List<Paciente> listPac = personaDao.deserializarPaciente(titulo);
            return listPac != null ? listPac.toString() : MensajeUtil.MENSAJE_ERROR + "ASEGURESE DE QUE SEAN PACIENTES";
        } catch (IOException ex) {
            return MensajeUtil.MENSAJE_ERROR + ex.getMessage();
        } catch (ClassNotFoundException ex) {
            return MensajeUtil.MENSAJE_ERROR + ex.getMessage();
        }        
    }

    @Override
    public String serializarDoctor(String titulo) {
        try {
            personaDao.serializarDoctor(titulo);
            return MensajeUtil.MENSAJE_OK;
        } catch (IOException ex) {
            return MensajeUtil.MENSAJE_ERROR + ex.getLocalizedMessage();
        }
    }

    @Override
    public String deserializarDoctor(String titulo) {        
        try {
            List<Doctor> listDoc = personaDao.deserializarDoctor(titulo);
            return listDoc != null ? listDoc.toString() : MensajeUtil.MENSAJE_ERROR + "ASEGURESE DE QUE SEAN DOCTORES";
        } catch (IOException ex) {
            return MensajeUtil.MENSAJE_ERROR + ex.getMessage();
        } catch (ClassNotFoundException ex) {
            return MensajeUtil.MENSAJE_ERROR + ex.getMessage();
        }  
    }   
    
}
