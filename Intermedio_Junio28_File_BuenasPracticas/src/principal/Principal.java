package principal;

import javax.swing.JOptionPane;
import negocio.HospitalControlador;
import negocio.IHospitalControlador;

/**
 * Clase Principal. Encontraremos todo lo
 * que el usuario verá por pantalla.
 *
 * @author fnang
 */
public class Principal {
    
    private static IHospitalControlador hospitalCtrl;
    private static final String FORMATO_MSJ = ".txt";
    private static final String FORMATO_SERIAL = ".dat";
    private static Integer ES_ARCH = 0;
    private static Integer ES_SERIAL = 1;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        hospitalCtrl = new HospitalControlador();
        menuDeOpciones();
    }

    public static void menuDeOpciones() {
        int opciones;

        do {
            System.out.println("================================");
            System.out.println("1 - Crear archivo");
            System.out.println("2 - Leer un archivo");
            System.out.println("3 - Serializar Paciente");
            System.out.println("4 - Deserializar Paciente");
            System.out.println("5 - Serializar Doctores");
            System.out.println("6 - Deserializar Doctores");
            System.out.println("7 - Salir");
            System.out.println("================================");
            System.out.println("Ingresar opcion: ");
            opciones = Consola.readInt();

            switch (opciones) {
                case 1:                     
                    imprimirPorConsola(hospitalCtrl.crearArchivo(ingresarTitulo(ES_ARCH), ingresarContenido()));
                    break;
                case 2:    
                    imprimirPorConsola(hospitalCtrl.leerArchivo(ingresarTitulo(ES_ARCH)));
                    break;
                case 3:  
                    imprimirPorConsola(hospitalCtrl.serializarPaciente(ingresarTitulo(ES_SERIAL)));
                    break;
                case 4:   
                    imprimirPorConsola(hospitalCtrl.deserializarPaciente(ingresarTitulo(ES_SERIAL)));
                    break;
                case 5:
                    imprimirPorConsola(hospitalCtrl.serializarDoctor(ingresarTitulo(ES_SERIAL)));
                    break;
                case 6:
                    imprimirPorConsola(hospitalCtrl.deserializarDoctor(ingresarTitulo(ES_SERIAL)));
                    break;
                case 7:
                    JOptionPane.showMessageDialog(null, "Muchas gracias por utilizar el programa.", "Adios!!", JOptionPane.INFORMATION_MESSAGE);
                    break; 
            }

        } while (opciones != 7);
    } //Fin del metodo menuDeOpciones()
    
    private static String ingresarTitulo(Integer tipo){        
        String titulo = JOptionPane.showInputDialog(null, "Ingresar SOLO EL TÍTULO del archivo, sin el formato: ", "INGRESAR", JOptionPane.INFORMATION_MESSAGE);
        return tipo == ES_ARCH ? titulo.concat(FORMATO_MSJ) : titulo.concat(FORMATO_SERIAL);
    }
    
    private static String ingresarContenido(){
        return JOptionPane.showInputDialog(null, "Ingresar el CONTENIDO del archivo: ", "INGRESAR", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private static void imprimirPorConsola(String mensaje){
        System.out.println(mensaje);
    }

} //Fin de la clase
