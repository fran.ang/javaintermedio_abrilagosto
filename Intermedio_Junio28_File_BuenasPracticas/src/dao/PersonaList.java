package dao;

import entidades.Doctor;
import entidades.Paciente;
import java.util.ArrayList;
import java.util.List;
import entidades.Persona;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Carga en memoria por ArrayList que implanta un DAO.
 *
 * @author Angonoa Franco
 */
public class PersonaList implements IPersonaDAO {

    private List<Persona> arregloPaciente;
    private List<Persona> arregloDoctor;
    private static final Path RUTA = Paths.get(".\\files");
    private static final Path RUTA_SER = Paths.get(".\\serializacion");

    public PersonaList() {
        arregloPaciente = new ArrayList<>();
        arregloDoctor = new ArrayList<>();
    }

    @Override
    public void crearArchivo(String titulo, String contenido) throws IOException {

        validarDirectorio(RUTA);
        
        File archivo = generarArchivo(RUTA, titulo);
        
        if(Files.exists(archivo.toPath(), LinkOption.NOFOLLOW_LINKS)){
            throw new IOException("EL ARCHIVO YA EXISTE");
        }

        //Para cerrar los recursos usamos try-with-resource
        try (
                FileWriter fw = new FileWriter(archivo); 
                BufferedWriter bw = new BufferedWriter(fw);) {
            bw.write(contenido);
            bw.newLine();
        }  
        
    }

    @Override
    public String leerArchivo(String titulo) throws IOException {
        String salida = "";
        
        File archivo = generarArchivo(RUTA, titulo);
        
        //Para cerrar los recursos usamos try-with-resource
        try (FileReader fr = new FileReader(archivo); 
                BufferedReader br = new BufferedReader(fr);) {

            String cadena;
            while ((cadena = br.readLine()) != null) {
                salida += cadena + "\n";
            }
        }
        
        return salida;
    }

    @Override
    public void serializarPaciente(String titulo) throws IOException{
        arregloPaciente.add(new Paciente(335, 37888888, "Bernardino Rivadavia"));
        arregloPaciente.add(new Paciente(337, 37999999, "Justo Jose de Urquiza"));
        arregloPaciente.add(new Paciente(339, 37101010, "Santiago Derqui"));
        
        validarDirectorio(RUTA_SER);
        
        serializarObjeto(RUTA_SER, titulo, arregloPaciente);                       
    }

    @Override
    public List<Paciente> deserializarPaciente(String titulo) throws IOException, ClassNotFoundException {
        return deserializarObjeto(titulo, Paciente.class);
    }

    @Override
    public void serializarDoctor(String titulo) throws IOException {
        arregloDoctor.add(new Doctor(113, 30123456, "Bartolome Mitre"));
        arregloDoctor.add(new Doctor(161, 31987654, "Domingo F. Sarmiento"));
        arregloDoctor.add(new Doctor(181, 32456789, "Nicolas Avellaneda"));
        
        validarDirectorio(RUTA_SER);
        
        serializarObjeto(RUTA_SER, titulo, arregloDoctor);               
    }

    @Override
    public List<Doctor> deserializarDoctor(String titulo) throws IOException, ClassNotFoundException{                
        return deserializarObjeto(titulo, Doctor.class);     
    }
    
    private void validarDirectorio(Path ruta) throws IOException{
        //Si es la primera vez, crear el directorio
        if( !Files.exists(ruta, LinkOption.NOFOLLOW_LINKS) ){
            Files.createDirectory(ruta);
        }
    }
    
    private File generarArchivo(Path ruta, String titulo){
        Path rutaArchivo = ruta.resolve(titulo);
        return rutaArchivo.toFile();
    }
    
    private void serializarObjeto(Path ruta, String titulo, List<Persona> listaObjetos) throws IOException{
        validarDirectorio(ruta);

        File archivo = generarArchivo(ruta, titulo);

        //Para cerrar los recursos usamos try-with-resource
        try (
                FileOutputStream fileOut = new FileOutputStream(archivo); 
                ObjectOutputStream objOut = new ObjectOutputStream(fileOut)) {
            objOut.writeObject(listaObjetos);
        }
    }
    
    private <T> List<T> deserializarObjeto(String titulo, Class<T> clase) throws IOException, ClassNotFoundException{        

        File archivo = generarArchivo(RUTA_SER, titulo);

        //Para cerrar los recursos usamos try-with-resource
        try (
                FileInputStream fileIn = new FileInputStream(archivo); 
                ObjectInputStream objIn = new ObjectInputStream(fileIn);) {
            
            List<T> lista = (List<T>) objIn.readObject();
            
            //Se agrega el control de clase para que los metodos
            //devuelvan el tipo que les corresponde
            return !lista.isEmpty() && lista.get(0).getClass().equals(clase) ? lista : null;
        }
    }

}
