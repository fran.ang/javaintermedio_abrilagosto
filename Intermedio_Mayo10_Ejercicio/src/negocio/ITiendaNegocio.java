
package negocio;

import entidades.Componente;

public interface ITiendaNegocio {
 
    void setComponente(Componente comp);
    
    void actualizarPrecios();
    
    String filtrarPorMarca(String marca);
    
    String filtrarPorCantTeclas(int cantTeclas);
    
    String cambiarMarca(int codBarra, String nuevaMarca);
    
    String cambiarCantTeclas(int codBarra,int nvaCantTeclas); 
}
