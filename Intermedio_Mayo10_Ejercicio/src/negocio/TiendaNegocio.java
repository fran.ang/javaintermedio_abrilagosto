
package negocio;

import datos.AccesoDatosDAO;
import datos.TiendaDatos;
import entidades.Componente;

public class TiendaNegocio implements ITiendaNegocio{
    
    private final AccesoDatosDAO tienda;
    
    public TiendaNegocio(){
        tienda = TiendaDatos.getInstancia(6);
    }

    @Override
    public void setComponente(Componente comp) {
        if(comp != null){
            tienda.setComponente(comp);
        }        
    }

    @Override
    public void actualizarPrecios() {
        tienda.actualizarPrecios();
    }

    @Override
    public String filtrarPorMarca(String marca) {        
        return marca != null && !marca.isEmpty() ? 
                tienda.filtrarPorMarca(marca) : 
                "Debe ingresar un nombre de marca";        
    }

    @Override
    public String filtrarPorCantTeclas(int cantTeclas) {
        return cantTeclas > 0 ? 
                tienda.filtrarPorCantTeclas(cantTeclas) : 
                "Debe ingresar un numero positivo";
    }

    @Override
    public String cambiarMarca(int codBarra, String nuevaMarca) {
        return codBarra > 0 && nuevaMarca != null ? 
                tienda.cambiarMarca(codBarra, nuevaMarca) : 
                "Debe ingresar datos validos";
    }

    @Override
    public String cambiarCantTeclas(int codBarra, int nvaCantTeclas) {
        return codBarra > 0 && nvaCantTeclas > 0 ?
                tienda.cambiarCantTeclas(codBarra, nvaCantTeclas) :
                "Debe ingresar datos validos";
    }
    
    @Override
    public String toString(){
        return tienda.consultarComponentes();        
    }
}
