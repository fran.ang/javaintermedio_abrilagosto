
package datos;

import entidades.Componente;

public interface AccesoDatosDAO {
    
    int MAX_COMP = 4;
    
    int getCantidadComponentes();
    
    void setComponente(Componente comp);
    
    void actualizarPrecios();
    
    String filtrarPorMarca(String marca);
    
    String filtrarPorCantTeclas(int cantTeclas);
    
    String cambiarMarca(int codBarra, String nuevaMarca);
    
    String cambiarCantTeclas(int codBarra,int nvaCantTeclas); 
    
    String consultarComponentes();
    
}
