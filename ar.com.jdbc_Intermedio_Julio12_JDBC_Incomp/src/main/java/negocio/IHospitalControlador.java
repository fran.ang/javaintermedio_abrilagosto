package negocio;

import entidades.Persona;

/**
 *
 * @author franc
 */
public interface IHospitalControlador {
    
    String agregarPersona(Persona persona);
    
    String modificarPersona(int documento, String nuevoNombre);
    
    String borrarPersona(int documento);
    
    String buscarPorDNI(int documento);
    
    String obtenerTodasPersonas();
}
