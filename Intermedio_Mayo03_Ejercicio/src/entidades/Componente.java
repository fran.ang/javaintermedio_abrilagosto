package entidades;

import constantes.EstadosComp;
import java.util.Objects;

public abstract class Componente {
    //ATRIBUTOS GLOBALES
    private int codigoDeBarra;
    private String marca;
    private String pais;
    private float precio;
    private static int contadorComp;
    private EstadosComp estado;

    //CONSTRUCTORES
    protected Componente() {
        this.codigoDeBarra = ++contadorComp;
    }

    public Componente(String marca, String pais, float precio, EstadosComp estado) {
        this();
        this.marca = marca;
        this.pais = pais;
        this.precio = precio;
        this.estado = estado;
    }
       
    //METODOS
    public int getCodigoDeBarra() {
        return this.codigoDeBarra;
    }

    public void setCodigoDeBarra(int codigoDeBarra) {
        this.codigoDeBarra = codigoDeBarra;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public EstadosComp getEstado() {
        return estado;
    }

    public void setEstado(EstadosComp estado) {
        this.estado = estado;
    }
    
    
    
    public abstract void actualizarPrecio();


    @Override
    public String toString() {
        return "Componente { Cod. de barra: " + codigoDeBarra
                + ", marca: " + marca
                + ", pais de fab.: " + pais
                + ", precio: " + precio + "$"
                + ", estado: " + estado + " }";
    }

}
