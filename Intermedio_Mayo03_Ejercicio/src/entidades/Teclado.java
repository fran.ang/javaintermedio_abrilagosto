package entidades;

import constantes.EstadosComp;

public class Teclado extends Componente{
    
    //ATRIBUTO DE LA CLASE
    private int cantidadTeclas;
    
    //CONSTRUCTORES

    public Teclado() {
        //super();
    }

    public Teclado(int cantidadTeclas, String marca, 
            String pais, float precio, EstadosComp estado) {
        super(marca, pais, precio, estado);
        this.cantidadTeclas = cantidadTeclas;
    }
    
    //METODOS

    public int getCantidadTeclas() {
        return cantidadTeclas;
    }

    public void setCantidadTeclas(int cantidadTeclas) {
        this.cantidadTeclas = cantidadTeclas;
    }

    @Override
    public String toString() {
        return super.toString() + " - Es TECLADO{" + "Cantidad de teclas: " 
                + cantidadTeclas + '}';
    }

    @Override
    public void actualizarPrecio() {
        //Si es teclado, agregar un impuesto del 10%
        float impuesto = getPrecio() * 0.1f;
        setPrecio(getPrecio() + impuesto);
    }
    
    
    
}
