package constantes;

public enum EstadosComp {
    NUEVO, REPARANDO, VENDIDO, EN_ADUANA, DEVUELTO
}
