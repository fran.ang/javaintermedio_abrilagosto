package negocio;

import entidades.*;

/**
 * Clase que representa a la tienda del negocio.
 * Esta clase contiene todos los metodos de logica del negocio.
 * 
 * @author Franco
 * @version 1.1
 * 
 */
public class Tienda {
    //ATRIBUTOS GLOBALES
    
    private String nombre;
    private Componente[] componentes;
    public static final int MAX_COMP = 4;
    private int contadorComp; 
    private static Tienda instancia;
    
    //CONSTRUCTORES
    /**
     * Consctructor por defecto.
     * Este constructor se carga un objeto por defecto
     * con un nombre, y con dos teclados y mouse.
     */
    public Tienda(){
        nombre = "Informatica Cordoba";
        componentes = new Componente[MAX_COMP];        
    }
    
    /**
     * Constructor con dos parametros.
     * Crea un arreglo de componentes de mouse de n elementos, y uno de 
     * teclados de m elementos.      
     * @param n el tamanio del arreglo de componentes.
     */
    private Tienda(final int n){
        nombre = "Informatica Cordoba";        
        componentes = new Componente[n];        
    }
    
    /**
     * Metodo para el patron Singleton
     */
    public static Tienda getInstancia(int n){
        if(instancia == null){
            instancia = new Tienda(n);
        }
        return instancia;
    }
        
    /**
     * Metodo que retorna la cantidad de componentes.
     * Solo retorna el length del arreglo componentes.
     * 
     * @return Numero entero con la cantidad de componentes.
     */
    public int getCantidadComponentes()
    {
        return componentes.length;   
    }

   
    public void setComponente(Componente comp)
    {
        if (contadorComp < getCantidadComponentes()) {
            componentes[contadorComp++] = comp;
        }
        else{
            System.out.println("Ya no se permite cargar más componentes");
        }
    }
    
    //METODOS PARA IMPLEMENTAR EL MENU DE OPCIONES
    public void actualizarPrecios(){
        for (Componente comp : componentes) {
            System.out.println("\n" + comp);
            System.out.println("\tPrecio anterior: " + comp.getPrecio());
            comp.actualizarPrecio();
            System.out.println("\tPrecio nuevo: " + comp.getPrecio());
        }               
    }
    
    /**
     * Filtrar por marca.
     * Este metodo filtra los componentes por una marca
     * ingresada por el usuario.
     * 
     * @param marca Representa la marca a filtrar
     * @return Una cadena de componentes filtradas por marca
     */
    public String filtrarPorMarca(String marca){
        String cadena = "COMPONENTES FILTRADOS POR: " + marca + "\n";
        int contador = 0;
        
        for (Componente comp : componentes) {
            if(comp.getMarca().equalsIgnoreCase(marca)){
                cadena += comp + "\n";
                contador++;
            }
        }                
        
        if(contador == 0){
            cadena += "La marca no existe en la tienda";
        }
        
        return cadena;
    }
    
    /**
     * Filtrar teclados según la cantidad de teclas solicitada.
     * @param cantTeclas
     * @return cadena con los teclads mayores a una cantidad de teclas
     */
    public String filtrarPorCantTeclas(int cantTeclas){
        String cadena = "===COMPONENTES FILTRADOS POR CANT. DE TECLAS MAYOR A : " + cantTeclas + "===\n\t";
        
        for (Componente comp : componentes) {
            if(comp instanceof Teclado){
                if(((Teclado) comp).getCantidadTeclas() > cantTeclas){
                    cadena += comp + "\n\t";
                }
            }
        }
        
        return cadena;
    }
    
    /**
     * Buscar un mouse por su código de barra y cambiar la marca.
     * @param codBarra
     * @param nuevaMarca
     * @return informacion del estado de cambio de marca
     */
    public String cambiarMarca(int codBarra, String nuevaMarca){
        String rpta = "No se ha encontrado el mouse";
        
        for (Componente comp : componentes) {
            if (comp instanceof Mouse) {
                if (comp.getCodigoDeBarra() == codBarra) {
                    comp.setMarca(nuevaMarca);
                    rpta = "Se encontro el mouse y su marca se modifico con exito.";
                    break;
                }
            }
        }
                
        return rpta;
    }
    
    /**
     * Buscar un teclado por su código de barra y cambiar la cantidad de teclas.
     * @param codBarra
     * @param nvaCantTeclas
     * @return cadena informando el nuevo cambio de teclas
     */
    public String cambiarCantTeclas(int codBarra,int nvaCantTeclas){
        String rpta = "No se ha encontrado el teclado";
        
        for (Componente comp : componentes) {            
            if(comp instanceof Teclado){
                if(comp.getCodigoDeBarra() == codBarra){
                    ((Teclado) comp).setCantidadTeclas(nvaCantTeclas);
                    rpta = "Se encontro el teclado y su cantidad de teclas se modifico con exito.";
                    break;  
                }
            }
        }
        
        return rpta;
    }
    
    @Override
    public String toString(){
        String cadena = "Tienda: " + nombre + "\nMouse disponibles:\n\t";

        //Recorrer arreglo mouses
        for (Componente comp : componentes) {
            if(comp instanceof Mouse){
                cadena += comp + "\n\t";
            }
        }
        
        cadena += "\nTeclados disponibles:\n\t";
        
        //Recorrer arreglo teclados
        for (Componente comp : componentes) {
            if (comp instanceof Teclado) {
                cadena += comp + "\n\t";
            }
        }
        
        return cadena;
    }
       
    
}//Fin de la clase
