package dao;

import entidades.Doctor;
import entidades.Paciente;
import java.util.ArrayList;
import java.util.List;
import entidades.Persona;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Carga en memoria por ArrayList que implanta un DAO.
 *
 * @author Angonoa Franco
 */
public class PersonaList implements IPersonaDAO {

    private List<Persona> arregloPaciente;
    private List<Persona> arregloDoctor;
    private static final Path RUTA = Paths.get(".\\files");
    private static final Path RUTA_SER = Paths.get(".\\serializacion");

    public PersonaList() {
        arregloPaciente = new ArrayList<>();
        arregloDoctor = new ArrayList<>();
    }

    @Override
    public void crearArchivo(String titulo, String contenido) throws IOException {

        //Si es la primera vez, crear el directorio
        //Path ruta = Paths.get(".\\files");
        if( !Files.exists(RUTA, LinkOption.NOFOLLOW_LINKS) ){
            Files.createDirectory(RUTA);
        }
        
        //En el metodo serializarPaciente esta mejorado la ruta
        File archivo = new File(RUTA + "\\" + titulo);
        
        if(Files.exists(archivo.toPath(), LinkOption.NOFOLLOW_LINKS)){
            throw new IOException("EL ARCHIVO YA EXISTE");
        }

        //Para cerrar los recursos usamos try-with-resource
        try (
                FileWriter fw = new FileWriter(archivo); 
                BufferedWriter bw = new BufferedWriter(fw);) {
            bw.write(contenido);
            bw.newLine();
        }     
        
    }

    @Override
    public String leerArchivo(String titulo) throws IOException {
        String salida = "";
        
        //En el metodo deserializarPaciente esta mejorado la ruta
        File archivo = new File(RUTA + "\\" + titulo);
        
        FileReader fr = new FileReader(archivo);
        BufferedReader br = new BufferedReader(fr);
        
        String cadena;
        while( (cadena = br.readLine()) != null ){
            salida += cadena + "\n";
        }
        
        //Cerrar los recursos con metodos close()
        br.close();
        fr.close();
        
        return salida;
    }

    @Override
    public void serializarPaciente(String titulo) throws IOException{
        arregloPaciente.add(new Paciente(335, 37888888, "Bernardino Rivadavia"));
        arregloPaciente.add(new Paciente(337, 37999999, "Justo Jose de Urquiza"));
        arregloPaciente.add(new Paciente(339, 37101010, "Santiago Derqui"));
        
        if( !Files.exists(RUTA_SER, LinkOption.NOFOLLOW_LINKS) ){
            Files.createDirectory(RUTA_SER);
        }

        Path rutaArchivo = RUTA_SER.resolve(titulo);
        File archivo = rutaArchivo.toFile();
        
        //Para cerrar los recursos usamos try-with-resource
        try (
                FileOutputStream fileOut = new FileOutputStream(archivo);
                ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
                ) {
            objOut.writeObject(arregloPaciente);
        }
        
    }

    @Override
    public List<Paciente> deserializarPaciente(String titulo) throws IOException, ClassNotFoundException {
        List<Paciente> listaPacientes = null;
        
        Path rutaArchivo = RUTA_SER.resolve(titulo);
        File archivo = rutaArchivo.toFile();
        
        //Para cerrar los recursos usamos try-with-resource
        try (
                FileInputStream fileIn = new FileInputStream(archivo);
                ObjectInputStream objIn = new ObjectInputStream(fileIn);
                ) {
            listaPacientes = (List<Paciente>) objIn.readObject();
        }
        
        return listaPacientes;
    }

    @Override
    public void serializarDoctor(String titulo) throws IOException {
        arregloDoctor.add(new Doctor(113, 30123456, "Bartolome Mitre"));
        arregloDoctor.add(new Doctor(161, 31987654, "Domingo F. Sarmiento"));
        arregloDoctor.add(new Doctor(181, 32456789, "Nicolas Avellaneda"));
        
        if( !Files.exists(RUTA_SER, LinkOption.NOFOLLOW_LINKS) ){
            Files.createDirectory(RUTA_SER);
        }

        Path rutaArchivo = RUTA_SER.resolve(titulo);
        File archivo = rutaArchivo.toFile();
        
        FileOutputStream fileOut = new FileOutputStream(archivo);
        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
        
        objOut.writeObject(arregloDoctor);
        
        //Cierre flujo de salida
        objOut.flush();
        fileOut.close();
    }

    @Override
    public List<Doctor> deserializarDoctor(String titulo) throws IOException, ClassNotFoundException{
        List<Doctor> listaDoctores = null;
        
        Path rutaArchivo = RUTA_SER.resolve(titulo);
        File archivo = rutaArchivo.toFile();
       
        FileInputStream fileIn = new FileInputStream(archivo);
        ObjectInputStream objIn = new ObjectInputStream(fileIn);

        listaDoctores = (List<Doctor>) objIn.readObject();

        //Para cerrar los recursos usamos el metodo close()
        objIn.close();
        fileIn.close();

        return listaDoctores;
    }

}
