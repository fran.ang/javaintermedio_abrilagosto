 
package dao;

import entidades.Doctor;
import entidades.Paciente;
import java.io.IOException;
import java.util.List;

/**
 * Interfaz que respeta el patron DAO.
 * Donde todas aquellas clases que lo implanten deberan cumplir
 * con los metodos y sus respectivas firmas.
 * @author fnang
 */
public interface IPersonaDAO {
    
    void crearArchivo(String titulo, String contenido) throws IOException;
    
    String leerArchivo(String titulo) throws IOException ;
        
    void serializarPaciente(String titulo) throws IOException;
    
    List<Paciente> deserializarPaciente(String titulo) throws IOException, ClassNotFoundException;
       
    void serializarDoctor(String titulo) throws IOException;
    
    List<Doctor> deserializarDoctor(String titulo) throws IOException, ClassNotFoundException;
}
