package principal;

import javax.swing.JOptionPane;
import negocio.HospitalControlador;
import negocio.IHospitalControlador;

/**
 * Clase Principal. Encontraremos todo lo
 * que el usuario verá por pantalla.
 *
 * @author fnang
 */
public class Principal {
    
    private static IHospitalControlador hospital;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        hospital = new HospitalControlador();
        menuDeOpciones();
    }

    public static void menuDeOpciones() {
        int opciones;

        do {
            System.out.println("================================");
            System.out.println("1 - Crear archivo");
            System.out.println("2 - Leer un archivo");
            System.out.println("3 - Serializar Paciente");
            System.out.println("4 - Deserializar Paciente");
            System.out.println("5 - Serializar Doctores");
            System.out.println("6 - Deserializar Doctores");
            System.out.println("7 - Salir");
            System.out.println("================================");
            System.out.println("Ingresar opcion: ");
            opciones = Consola.readInt();

            switch (opciones) {
                case 1: 
                    System.out.println("Ingresar el titulo del archivo: ");
                    String titulo = Consola.readLine();
                    System.out.println("Ingresar el conenido del arhivo: ");
                    String contenido = Consola.readLine();
                    String crearArchivo = hospital.crearArchivo(titulo, contenido);
                    System.out.println(crearArchivo);
                    break;
                case 2:    
                    System.out.println("Ingresar el titulo del archivo: ");
                    String titulo2 = Consola.readLine();
                    String leerArchivo = hospital.leerArchivo(titulo2);
                    System.out.println(leerArchivo);
                    break;
                case 3:  
                    System.out.println("Ingresar el titulo del archivo: ");
                    String titulo3 = Consola.readLine();
                    System.out.println(hospital.serializarPaciente(titulo3));
                    break;
                case 4:   
                    System.out.println("Ingresar el titulo del archivo: ");
                    String titulo4 = Consola.readLine();
                    System.out.println(hospital.deserializarPaciente(titulo4));
                    break;
                case 5:
                    System.out.println("Ingresar el titulo del archivo: ");
                    String titulo5 = Consola.readLine();
                    System.out.println(hospital.serializarDoctor(titulo5));
                    break;
                case 6:
                    System.out.println("Ingresar el titulo del archivo: ");
                    String titulo6 = Consola.readLine();
                    System.out.println(hospital.deserializarDoctor(titulo6));
                    break;
                case 7:
                    JOptionPane.showMessageDialog(null, "Muchas gracias por utilizar el programa.", "Adios!!", JOptionPane.INFORMATION_MESSAGE);
                    break; 
            }

        } while (opciones != 7);
    } //Fin del metodo menuDeOpciones()

} //Fin de la clase
