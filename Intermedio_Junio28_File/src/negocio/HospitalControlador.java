package negocio;

import dao.IPersonaDAO;
import dao.PersonaList;
import entidades.Doctor;
import entidades.Paciente;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author franc
 */
public class HospitalControlador implements IHospitalControlador{
    
    private IPersonaDAO personaDao;

    public HospitalControlador() {
        personaDao = new PersonaList();
    }        

    @Override
    public String crearArchivo(String titulo, String contenido) {
        
        try {
            personaDao.crearArchivo(titulo, contenido);
            return "SE CREO EL ARCHIVO CON EXITO";
        } catch (IOException ex) {
            return "OCURRIO UN PROBLEMA AL CREAR EL ARCHIVO: " + ex.getMessage();
        }
        
    }

    @Override
    public String leerArchivo(String titulo) {        
        try {
            return personaDao.leerArchivo(titulo);
        } catch (IOException ex) {
            return "OCURRIO UN PROBLEMA AL LEER EL ARCHIVO: " + ex.getMessage();
        }
    }

    @Override
    public String serializarPaciente(String titulo) {
        try {
            personaDao.serializarPaciente(titulo);
            return "SE SERIALIZO LA LISTA DE PACIENTES CON EXITO.";
        } catch (IOException ex) {
            return "OCURRIO UN ERROR AL SERIALIZAR: " + ex.getLocalizedMessage();
        }
    }

    @Override
    public String deserializarPaciente(String titulo) {
        final String MSJE_ERROR = "OCURRIO UN ERROR AL DESERIALIZAR: ";
        
        try {
            List<Paciente> listPac = personaDao.deserializarPaciente(titulo);
            return listPac.toString();
        } catch (IOException ex) {
            return MSJE_ERROR + ex.getMessage();
        } catch (ClassNotFoundException ex) {
            return MSJE_ERROR + ex.getMessage();
        }        
    }

    @Override
    public String serializarDoctor(String titulo) {
        try {
            personaDao.serializarDoctor(titulo);
            return "SE SERIALIZO LA LISTA DE DOCTORES CON EXITO.";
        } catch (IOException ex) {
            return "OCURRIO UN ERROR AL SERIALIZAR: " + ex.getLocalizedMessage();
        }
    }

    @Override
    public String deserializarDoctor(String titulo) {
        final String MSJE_ERROR = "OCURRIO UN ERROR AL DESERIALIZAR: ";
        
        try {
            List<Doctor> listDoc = personaDao.deserializarDoctor(titulo);
            return listDoc.toString();
        } catch (IOException ex) {
            return MSJE_ERROR + ex.getMessage();
        } catch (ClassNotFoundException ex) {
            return MSJE_ERROR + ex.getMessage();
        }  
    }   
    
}
