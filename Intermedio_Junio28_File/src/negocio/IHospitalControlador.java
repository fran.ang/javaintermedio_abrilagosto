package negocio;

import entidades.Persona;

/**
 *
 * @author franc
 */
public interface IHospitalControlador {
    
    String crearArchivo(String titulo, String contenido);
    
    String leerArchivo(String titulo);
    
    String serializarPaciente(String titulo);
    
    String deserializarPaciente(String titulo);
       
    String serializarDoctor(String titulo);
    
    String deserializarDoctor(String titulo);    
}
